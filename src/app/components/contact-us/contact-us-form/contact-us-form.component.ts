import { Component, OnInit } from '@angular/core';
import { ContactUs } from 'src/app/models/contact-us.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-contact-us-form',
  templateUrl: './contact-us-form.component.html',
  styleUrls: ['./contact-us-form.component.scss']
})
export class ContactUsFormComponent implements OnInit {

  model = new ContactUs();
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  clearModel() {
    this.model = new ContactUs();
  }

  submitForm() {
    this._snackBar.open('Successfully send message', 'Ok', {
      duration: 3000,
      verticalPosition: 'top',
      horizontalPosition: 'right'
    });
    this.clearModel();
  }

}

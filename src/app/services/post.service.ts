import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, } from 'rxjs/operators';

import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private firestore: AngularFirestore) { }

  /**
   * Get posts from fire base use the Post model to return it as an Observable<Post[]>
   *
   * @returns Observable<Post[]>
   */
  getPosts(): Observable<Post[]> {
    return this.firestore.collection<Post>('posts').snapshotChanges()
    .pipe(
      map(res => {
        return res.map(data => {
          return {
            id: data.payload.doc.id,
            ...data.payload.doc.data()
          } as Post
        })
      })
    );
  }

  /**
   * Get post by id
   *
   * @param id
   * @returns Observable<Post>
   */
  getPostById(id: string): Observable<Post> {
    return this.firestore.collection('posts').doc(id).valueChanges()
    .pipe(
      map(res => {
        return res as Post;
      })
    )
  }

  /**
   * Create a post with parameter Post model
   *
   * @param post
   */
  addPost(post: any) {
    return new Promise<any>((resolve, reject) =>{
      this.firestore
          .collection('posts')
          .add(Object.assign({}, post))
          .then(res => {
            resolve(res);
          }, err => reject(err));
  });
  }
}

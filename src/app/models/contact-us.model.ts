export class ContactUs {
  firstName: string;
  lastName: string;
  contactNumber: string;
  emailAddress: string;
  message: string;
}

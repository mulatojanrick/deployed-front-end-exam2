# Deployed Front End Exam 2

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Setup

```
npm install
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## App

Simple app using a firebase no sql database, fetching and inserting to collection posts.
* Reused the previous news components to be responsive in different layout desktop = 4 cards, iPad / tablet = 2 cards and mobile 1 card only.
* Create a simple contact us form to display a responsive input depending on layout.
* Used scss instead of css only for styling components.
